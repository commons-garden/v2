---
title: Initiatives
display: public
description: Our open initiatives
date: 2023-05-16
---

1.  invite-driven onboarding
    - [diagram](https://www.figma.com/file/eG3RjraaGHIdv1nuUp667J/Invite-driven-onboarding?node-id=0%3A1&t=R3PDlUca7gZLo0is-1) (figjam)
    - [documentation](https://gitlab.com/staltz/manyverse/-/issues/1887) (gitlab)
2.  p2p delete
    - [board](https://www.figma.com/file/a3MVrJ2Z8UDILkHK0NUeYb/p2p-delete?node-id=5%3A311&t=7mcjSfgX1w3FWexU-1) (figjam)
    - [documentation](https://hackmd.io/@praxis/p2p-delete) (hackmd)
    - [conversation](https://signal.group/#CjQKIGFSis4tbGdzpujg_Xw-UudxHZnnPuxmxPfFjSbzvII7EhAXQHs1aCqWBymiid6TMV78) (signal)
3.  identity recovery
    - [board](https://www.figma.com/file/Qkei1VgElbho66UinY9MzH/identity-recovery?node-id=0%3A1&t=X61ec705X2vYRHFz-1) (figjam)
    - [proposal](https://gitlab.com/staltz/manyverse/-/issues/1942) (gitlab)
4.  SSB moderation
    - [board](https://www.figma.com/file/C9YFMDwsOIXbmt88QoxL2B/SSB-moderation?node-id=0%3A1&t=F1bQPrqHYAd5pXwu-1)
    - [conversation](https://signal.group/#CjQKIKbXJYLBlMFQeAj5vSma4LFdEzmFhBnguL3AuOeJEclnEhBx3ApnFdng1LyS7ZR7l836)
    - [documentation](https://hackmd.io/@praxis/moderation)
5.  backlog ([board](https://www.figma.com/file/UIQDqYS48Pkg0ARSY9Rpf4/feature-backlog?node-id=0%3A1&t=uNvxo364L3hzJ2BL-1))
    - multi-device (fusion identity)
    - dying online
    - stencil
