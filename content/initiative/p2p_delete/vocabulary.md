---
title: Vocabulary
display: private
status: ongoing
description: Serverless irretrievable delete
date: 2023-03-07
---

- builds interfaces, wants to able to convey what's happening to users
- what are the different meanings of delete, do we need to be more specific
- values accuracy / precision in communication
- linguistic literalist, contortionist
- alignment of practices/ rituals
- opportunity to shape the words to reflect our desired practice
- archival versus privacy tensions
- language is a protocol for how we coordinate
  - meaning/ expectation
  - technical
  - forgetting
