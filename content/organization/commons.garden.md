---
title: commons.garden
display: public
description: middle-management for commons
date: 2024-04-12
members: [nonlinear, mixmix, spencer]
link: https://commons.garden
needs: [facilitation, community organizing, documentation]
---
