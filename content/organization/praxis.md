---
title: praxis
display: public
description: a not-for-profit design studio delivering tools for the commons
date: 2024-04-12
members: [nonlinear, lyndsey, camille]
link: https://praxis.nyc
---

Praxis is a small group of thinkers getting together for study groups, salons and soundboards.
